terraform {
  backend "gcs" {
    bucket = "tf-state-web-facing"
    prefix = "terraform/state"
  }
}
