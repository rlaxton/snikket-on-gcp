variable "debian_10_sku" {
  type        = string
  description = "SKU for Debian 10"
  default     = "debian-cloud/debian-10"
}

variable "debian_11_sku" {
  type        = string
  description = "SKU for Debian 11"
  default     = "debian-cloud/debian-11"
}

variable "debian_11_arm64_sku" {
  type        = string
  description = "SKU for Debian 11 arm64"
  default     = "debian-cloud/debian-11-arm64"
}
