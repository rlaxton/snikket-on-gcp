resource "google_compute_instance" "vm_instance_public" {
  name         = "snikket-vm-01"
  machine_type = var.linux_instance_type
  zone         = var.gcp_zone
  hostname     = "chat.killrobot.com"
  tags         = ["ssh", "xmpp"]

  boot_disk {
    initialize_params {
      image = var.debian_11_sku
    }
  }

  network_interface {
    network    = google_compute_network.vpc.name
    subnetwork = google_compute_subnetwork.network_subnet.name
    access_config {}
  }

  metadata_startup_script = file("/home/rlaxton/Documents/_Ongoing-Activities/Education/IT/terraform-projects/snikket-on-gcp/scripts/startup_script.sh")

  scheduling {
    preemptible       = false
    automatic_restart = true
  }

  #  metadata = {

  #    ssh-keys = join("\n", [for user, key in var.ssh_keys : "${user}:${key}"])
  #  }
}


#  provisioner "remote-exec" {
#    connection {
#      host        = google_compute_address.static.address
#      type        = "ssh"
#      user        = "${var.user}"
#      private_key = file(var.gcp_auth_file)
#      # private_key = file(var.privatekeypath) SSH key to GCP VM
#    }

#    inline = [
#      "sudo apt update", 
#      "sudo apt install -y ca-certificates",
#      "sudo apt install -y curl",
#      "sudo apt install -y gnupg",
#      "sudo apt install -y lsb-release",
#     "echo done installing Docker!"]

# sudo mkdir -p /etc/apt/keyrings
# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
# echo \
#  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
#  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# sudo apt-get update
# sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

# }


#  provisioner "local-exec" {
#    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' --private-key ${var.pvt_key} -e 'pub_key=${var.pub_key}' apache-install.yml"
#  }