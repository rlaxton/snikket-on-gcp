# Allow xmpp
resource "google_compute_firewall" "allow-xmpp" {
  name    = "snikket-fw-allow-xmpp"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["80", "443", "5222", "5269", "5000", "3478-3479", "5349-5350"]

  }

  allow {
    protocol = "udp"
    ports    = ["3478-3479", "5349-5350", "49152-65535"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["xmpp"]
}

# allow ssh
resource "google_compute_firewall" "allow-ssh" {
  name    = "snikket-fw-allow-ssh"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ssh"]
}