# GCP authentication file
variable "gcp_auth_file" {
  type        = string
  description = "GCP authentication file"
}

# define GCP region
variable "gcp_region" {
  type        = string
  description = "GCP region"
}

# define GCP zone
variable "gcp_zone" {
  type        = string
  description = "GCP zone"

}

# define GCP project name
variable "gcp_project_id" {
  type        = string
  description = "GCP project name"
}

# define Company name
variable "company" {
  type        = string
  description = "Name of the Company"

}

# define Application name
variable "app_name" {
  type        = string
  description = "Name of the application"
}

# define project stage
variable "environment" {
  type        = string
  description = "Dev, Test, Prod, etc"

}
