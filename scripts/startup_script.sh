#! /bin/bash
echo "Startup script started"
sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo mkdir /etc/snikket
sudo touch /etc/snikket/snikket.conf
sudo curl -o /etc/snikket/docker-compose.yml https://snikket.org/service/resources/docker-compose.beta.yml
sudo echo -e "# The primary domain of your Snikket instance\nSNIKKET_DOMAIN=chat.example.com\n\n# An email address where the admin can be contacted\n# (also used to register your Let's Encrypt account to obtain certificates)\nSNIKKET_ADMIN_EMAIL=you@example.com" >> /etc/snikket/snikket.conf

echo "Startup script complete"